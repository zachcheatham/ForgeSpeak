ForgeSpeak
==========
Overview
--------
ForgeSpeak is a simple server-side mod to display TeamSpeak 3 events in chat. 

Events that get displayed include:

*    Channel Connects / Disconnects
*    Channel Joins / Leaves
*    Channel Text Chat
*    Global Text Chat


Download
--------
All downloads can be found [here](https://bitbucket.org/zachcheatham/forgespeak/downloads/ "ForgeSpeak Downloads").

Usage / Permissions
-------------------
Do whatever you want.