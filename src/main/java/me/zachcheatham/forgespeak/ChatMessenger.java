package me.zachcheatham.forgespeak;

import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class ChatMessenger
{
	private static final String PREFIX = "[TS3] ";

	public static void showChatMessage(boolean global, String nick, String message)
	{
		StringBuilder broadcast = getNewSB();

		if (global)
		{
			broadcast.append(EnumChatFormatting.DARK_GREEN);
			broadcast.append("[G] ");
			broadcast.append(EnumChatFormatting.RESET);
		}

		broadcast.append("<");
		broadcast.append(nick);
		broadcast.append("> ");
		broadcast.append(handleBBCode(message));

		broadcastMessage(broadcast.toString());
	}

	public static void showConnect(String nick)
	{
		StringBuilder broadcast = getNewSB();

		broadcast.append(nick);
		broadcast.append(" connected to channel.");

		broadcastMessage(broadcast.toString());
	}

	public static void showDisconnect(String nick, String leavingMessage)
	{
		StringBuilder broadcast = getNewSB();

		broadcast.append(nick);
		broadcast.append(" disconnected (");
		broadcast.append(leavingMessage);
		broadcast.append(")");

		broadcastMessage(broadcast.toString());
	}

	public static void showMove(boolean joined, int moveReason, String nickname)
	{
		StringBuilder broadcast = getNewSB();
		broadcast.append(nickname);

		if (moveReason == 1)
			if (joined)
				broadcast.append(" was moved into the channel.");
			else
				broadcast.append(" was moved from the channel.");
		else
			if (joined)
				broadcast.append(" joined the channel.");
			else
				broadcast.append(" left the channel.");

		broadcastMessage(broadcast.toString());
	}

	public static void showServerLost()
	{
		StringBuilder broadcast = getNewSB();
		broadcast.append("Lost connection to TeamSpeak.");

		broadcastMessage(broadcast.toString());
	}

	public static void showServerConnected()
	{
		StringBuilder broadcast = getNewSB();
		broadcast.append("Connected to TeamSpeak.");

		broadcastMessage(broadcast.toString());
	}

	private static StringBuilder getNewSB()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(EnumChatFormatting.DARK_AQUA);
		sb.append(PREFIX);
		sb.append(EnumChatFormatting.RESET);

		return sb;
	}

	private static String handleBBCode(String message)
	{
		// We just remove BBCode for now
		return message.replaceAll("(\\[[\\w|/]+\\])", "");
	}

	private static void broadcastMessage(String message)
	{
		//FMLLog.log(ForgeSpeak.NAME, Level.INFO, message);
		MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(message));
	}
}
